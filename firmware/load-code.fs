\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>                                                
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited                                                                     
\ SPDX-License-Identifier: GPL-3.0-or-later                                                                                    
95 constant: DHT22                                                                                                             
98 constant: FLASH                                                                                                             
100 constant: FONT57                                                                                                           
102 constant: GPIO                                                                                                             
104 constant: MAILBOX                                                                                                          
105 constant: NETCON                                                                                                           
109 constant: NTP                                                                                                              
111 constant: PING                                                                                                             
113 constant: SONOFF                                                                                                           
115 constant: SSD1306I2C                                                                                                       
121 constant: SSD1306SPI                                                                                                       
128 constant: TASKS                                                                                                            
132 constant: TCPREPL                                                                                                          
135 constant: TURNKEY                                                                                                          
136 constant: WIFI                                                                                                             
138 constant: EVENT                                                                                                            
139 constant: RINGBUF                                                                                                          
142 constant: DECOMP                                                                                                           
145 constant: PUNIT                                                                                                            
148 constant: 1WIRE                                                                                                           
150 constant: DS18B20                                                                                                         
152 constant: TEMPSENS                                                                                                        
/end                                                                                                                           
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
83 load                                                                                                                       
81 load                                                                                                                       
TEMPSENS load                                                                                                                 
send-temp-cycle                                                                                                               
stack-show                                                                                                                    
/end                                                                                                                          
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              

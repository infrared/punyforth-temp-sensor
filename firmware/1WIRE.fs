\ 1WIRE.fs - This file is part of the Temperature Sensor project.                                                             
\ Released as free software by permission of Tatonduk Outfitters Limited.                                                     
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>                                               
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited                                                                    
\ SPDX-License-Identifier: GPL-3.0-or-later                                                                                   
\ public words:                                                                                                               
\   1w-open-pin 1w-reset-pulse 1w-sample 1w-init-cycle 1w-write-1 1w-write-0 1w-read 1w-cond-write                            
\   1w-write-byte-lsb 1w-read-byte-lsb 1w-skip-rom                                                                            
                                                                                                                              
GPIO load TASKS load                                                                                                          
                                                                                                                              
: 1w-open-pin ( pin# -- ) GPIO_OUT_OPEN_DRAIN gpio-mode ;                                                                     
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-reset-pulse ( pin# -- ) dup GPIO_LOW gpio-write 480 us GPIO_HIGH gpio-write ;                                            
                                                                                                                              
: 1w-sample ( pin# -- n ) dup dup GPIO_IN swap gpio-mode gpio-read swap 1w-open-pin ;                                         
                                                                                                                              
: presence-det ( pin# -- n ) 50 us dup 1w-sample 35 us swap 1w-sample or 395 us ;                                             
                                                                                                                              
: 1w-init-cycle ( pin# -- flag ) dup 1w-open-pin dup 1w-reset-pulse presence-det 0= if -1 else 0 then ;                       
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-write-1 ( pin# -- ) dup GPIO_LOW gpio-write 1 us GPIO_HIGH gpio-write 59 us ;                                            
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-write-0 ( pin# -- ) dup GPIO_LOW gpio-write 60 us GPIO_HIGH gpio-write 1 us ;                                            
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-read ( pin# -- n ) dup dup GPIO_LOW gpio-write 1 us GPIO_HIGH gpio-write 1 us 1w-sample 58 us ;                          
                                                                                                                              
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-cond-write ( pin# flag -- ) 0= if 1w-write-0 else 1w-write-1 then ;                                                      
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-write-byte-lsb ( pin# c -- ) 8 0 do 2dup 1 and 1w-cond-write 1 rshift loop 2drop ;                                       
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-read-byte-lsb ( pin# -- c ) 0 8 0 do 1 rshift over 1w-read 128 * or loop nip ;                                           
                                                                                                                              
\ assumes open drain pin                                                                                                      
: 1w-skip-rom ( pin# -- ) 204 1w-write-byte-lsb ;                                                                             
                                                                                                                              
/end                                                                                                                          
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              
                                                                                                                              

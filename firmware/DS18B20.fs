\ DS18B20.fs - This file is part of the Temperature Sensor project.                                                           
\ Released as free software by permission of Tatonduk Outfitters Limited                                                      
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>                                               
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited                                                                    
\ SPDX-License-Identifier: GPL-3.0-or-later                                                                                   
\ public-words:                                                                                                               
\   ds-tmp-read-cycle                                                                                                         
                                                                                                                              
1WIRE load                                                                                                                    
                                                                                                                              
\ assumes open drain pin                                                                                                      
: ds-convert ( pin# -- ) 68 1w-write-byte-lsb ;                                                                               
                                                                                                                              
\ assumes open drain pin                                                                                                      
: ds-conv-wait ( pin# -- ) begin dup 1w-read until drop ;                                                                     
                                                                                                                              
\ assumes open drain pin                                                                                                      
: ds-read-sp ( pin# -- ) 190 1w-write-byte-lsb ;                                                                              
                                                                                                                              
\ assumes open drain pin                                                                                                      
: ds-read-2bytes ( pin# -- c c ) dup 1w-read-byte-lsb swap 1w-read-byte-lsb ;                                                 
                                                                                                                              
\ assumes open drain pin                                                                                                      
: ds-tmp-read-cycle ( pin# -- c c flag | flag )                                                                               
  dup 1w-init-cycle 0= if drop 0 else                                                                                         
    dup 1w-skip-rom dup ds-convert dup ds-conv-wait                                                                           
    dup 1w-init-cycle 0= if drop 0 else                                                                                       
      dup 1w-skip-rom dup ds-read-sp ds-read-2bytes -1 then then ;                                                            
                                                                                                                              
/end                                                                                                                          
                                                                                                                              
                                                                                                                              

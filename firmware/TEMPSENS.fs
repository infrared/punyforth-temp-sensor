\ TEMPSENS.fs - This file is part of the Temperature Sensor project.                                                          
\ Released as free software by permission of Tatonduk Outfitters Limited                                                      
\ SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>                                               
\ SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited                                                                    
\ SPDX-License-Identifier: GPL-3.0-or-later                                                                                   
\ public words:                                                                                                               
\   sent-temp-cycle                                                                                                           
                                                                                                                              
DS18B20 load NETCON load                                                                                                      
                                                                                                                              
\ "************" "simsens" wifi-connect                                                                                       
"172.16.0.200" constant: SERVER_IP                                                                                            
23000 constant: SERVER_PORT                                                                                                   
4 constant: 1WPIN                                                                                                             
create: frame-mark 128 c, 255 c,                                                                                              
create: temp-buffer 0 c, 0 c,                                                                                                 
                                                                                                                              
: connect SERVER_PORT SERVER_IP UDP netcon-connect ;                                                                          
                                                                                                                              
: send-frame-mark ( conn -- ) frame-mark 2 netcon-send-buf ;                                                                  
                                                                                                                              
: send-temp-buffer ( conn -- ) temp-buffer 2 netcon-send-buf ;                                                                
                                                                                                                              
: fill-temp-buffer ( c c -- ) temp-buffer 1 + c! temp-buffer c! ;                                                             
                                                                                                                              
: send-temp-cycle ( -- ) connect 1WPIN 1w-open-pin begin 1WPIN ds-tmp-read-cycle if                                           
    fill-temp-buffer dup send-frame-mark dup send-temp-buffer 100 ms else                                                     
    1000 us then again ;                                                                                                      
                                                                                                                              
/end                                                                                                                          
                                                                                                                              
                                                                                                                              

# Copyright and Licensing

[![REUSE status](https://api.reuse.software/badge/codeberg.org/infrared/punyforth-temp-sensor)](https://api.reuse.software/info/codeberg.org/infrared/punyforth-temp-sensor)

README.md is part of the Temperature Sensor project

Released as free software by permission of Tatonduk Outfitters Limited

SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>

SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited

SPDX-License-Identifier: GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

# Project description

This project contains some Punyforth code to have an ESP-12E (ESP8266) read data from a DS18B20 and transmit it over Wi-Fi as UDP packets. There is also some primitive software to display the data on the receiving end.

This project is not really meant to be drop-in code suitable for any project, but is shared mainly to make available Punyforth words for 1-Wire bus communication, and DS18B20 communication, under a free software license. Please see firmware/1WIRE.fs and firmware/DS18B20.fs for this code.

# Requirements

## Hardware

- ESP-12E module (ESP8266). Our module had 4MB eeprom.
- DS18B20 temperature probe
- Single-drop 1-wire bus connection (non-parasitic power) from pin 4 (D2) to DS18B20 temperature probe. The 3.3V pin on the ESP-12E is sufficient for operation of the DS18B20. 5k pull-up resistor is sufficient for a short bus line, but 2k worked for a 50ft bus line.

## Punyforth firmware

We used punyforth pulled from

https://github.com/zeroflag/punyforth

commit 32a30163e3defaf231a286bcb56a430b8078996a

## Gnu/Linux software

Install Gnu Guix System, Stable release, for x86_64

- GNU Guile 3.0.7
- netcat (The GNU Netcat) 0.7.1
- Python 2.7.17

The following should work if using Guix x86_64 (https://guix.gnu.org/en/download/)

    $ guix pull --commit=5bdf1b6
    $ guix install guile netcat python2

I got errors in the Guile Scheme code when I tried to run it on 32-bit x86.

# Installing the firmware code

Install punyforth on the ESP-12E. From the punyforth/arch/esp8266/bin directory, run

    python flash.py /dev/ttyUSB0 --block-format true

After installing Punyforth on the ESP-12E, the following should work to install the code:

    esptool.py --port /dev/ttyUSB0 write_flash 606208 firmware/1WIRE.fs
    esptool.py --port /dev/ttyUSB0 write_flash 614400 firmware/DS18B20.fs
    esptool.py --port /dev/ttyUSB0 write_flash 331776 firmware/load-code.fs
    esptool.py --port /dev/ttyUSB0 write_flash 622592 firmware/TEMPSENS.fs

# Network connection

TEMPSENS.fs is coded to send UDP packets to 172.16.0.200:23000. Tweak as necessary.

Depending on your networking setup, you'll need to login to the microcontoller once and run "<some-password> "some-wifi-essid" wifi-connect to connect to your AP.

# Data packet format

In this setup, data is sent periodically in this 4-byte format:

80 FF (LSB) (MSB)

MSB and LSB are the raw 16-bits of temp data from the DS18B20, in 12-bit data mode (sign-extended). See the DS18B20 data sheet for more information.

# Running the GNU/Linux receiving software

    guile -l temp-disp.scm -c "(main)"
;; *********************************************************************
;;
;; temp-disp.scm is part of the Temperature Sensor project
;;
;; Released as free software by permission of Tatonduk Outfitters Limited
;;
;; SPDX-FileCopyrightText: 2021 Christopher Howard <christopher@librehacker.com>
;; SPDX-FileCopyrightText: 2021 Tatonduk Outfitters Limited
;; SPDX-License-Identifier: GPL-3.0-or-later
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;;
;; *********************************************************************

(use-modules (ice-9 binary-ports)
             (ice-9 format)
	     (ice-9 textual-ports)
	     (ice-9 popen)
             (ice-9 q)
             (srfi srfi-1)
             (srfi srfi-9))

(define devnull (%make-void-port "r+"))

(define (create-nc-pipe)
  (with-input-from-port (%make-void-port "r+")
    (lambda () (open-pipe "netcat -l -u -p 23000" OPEN_READ))))

(define-record-type <state>
  (make-state last-temp last-update-time initial-wait)
  state?
  (last-temp state-last-temp set-state-last-temp!)
  (last-update-time state-last-update-time set-state-last-update-time!)
  (initial-wait state-initial-wait set-state-initial-wait!))

(define (make-default-state) (make-state #f (current-time) #t))

(define (make-ibuff)
  (fold (lambda (v q) (enq! q v)) (make-q) '(0 0 0 0)))

(define (output-bytes lst)
  (let ((op (current-output-port)))
    (for-each (lambda (n) (put-u8 op n)) lst)))

(define (output-csi)
  (output-bytes (list #x1b #x5b)))

(define (ansi-start-of-line)
  (output-csi)
  (output-bytes (list #x31 #x47)))

(define (ansi-clear-screen)
  (output-csi)
  (output-bytes (list #x32 #x4a)))

(define (ansi-cursor-to-top-left)
  (output-csi)
  (output-bytes (list #x31 #x3b #x31 #x48)))

(define (ansi-clear-line)
  (output-csi)
  (output-bytes (list #x32 #x4b)))

(define (temp-data-valid? ibuff)
  (let ((raw-list (car ibuff)))
    (and (= (list-ref raw-list 0) #x80)
         (= (list-ref raw-list 1) #xff))))

(define (ibuff-to-celsius ibuff)
  (let* ((raw-list (car ibuff))
         (lsb (list-ref raw-list 2))
         (msb (list-ref raw-list 3))
         (sign-mul (if (< 0 (logand msb #x80)) -1 1))
         (adj-msb (ash (logand msb 7) 8))
         (comb-bits (logior adj-msb lsb))
         (degrees (* sign-mul 0.0625 comb-bits)))
    degrees))

(define (combine-bytes msb lsb)
  (+ (* msb 256) lsb))

(define (output-temp raw-celsius)
  (let ((rounded-celsius (inexact->exact (round raw-celsius))))
    (format #t "~3d °C (~3d °F)"
            rounded-celsius
            (inexact->exact (round (+ 32 (* 1.8 rounded-celsius)))))))

(define (output-status ibuff state)
  ;; (ansi-start-of-line)
  ;; (ansi-clear-line)
  (display ibuff) ;; DEBUG
  (display state) ;; DEBUG
  (display "\n") ;; DEBUG
  (if (or (state-initial-wait state) (not (state-last-temp state)))
      (display "WAITING FOR DATA")
      (if (> (- (current-time) (state-last-update-time state)) 2)
          (display "DATA STREAM LOST")
          (output-temp (state-last-temp state)))))

(define (restart-pipe-if-necessary nc-pipe state)
  (if (and (> (- (current-time) (state-last-update-time state)) 8)
           (not (state-initial-wait state)))
      (begin
	(kill (hash-ref port/pid-table nc-pipe) SIGTERM)
	(close-pipe nc-pipe)
	(sleep 1)
        (let ((new-pipe (create-nc-pipe)))
	  (setvbuf new-pipe 'none)
          (set-state-initial-wait! state #t)
	  new-pipe))
      #f))

;; (define (weighted-average old-value new-value)
;;   (if old-value
;;       (let ((old-weight 8))
;;         (/ (+ (* old-value old-weight) new-value) (+ old-weight 1)))
;;       new-value))

(define (event-loop nc-pipe ibuff state)
   (if (and (not (port-closed? nc-pipe)) (char-ready? nc-pipe))
       (begin
         (enq! ibuff (get-u8 nc-pipe))
	 (deq! ibuff)
         (set-state-initial-wait! state #f)
         (set-state-last-update-time! state (current-time))
         (if (temp-data-valid? ibuff)
             (set-state-last-temp!
              state
              (ibuff-to-celsius ibuff))))
       (let ((new-pipe (restart-pipe-if-necessary nc-pipe state)))
	 (if new-pipe (set! nc-pipe new-pipe))))
   (output-status ibuff state)
   (usleep 50000)
   (event-loop nc-pipe ibuff state))

(define (main)
  (let ((nc (create-nc-pipe)))
    (with-exception-handler
        (lambda (e)
          #;(close-pipe nc) (raise-exception e))
	(lambda ()
	  (setvbuf nc 'none)
          (ansi-clear-screen)
          (ansi-cursor-to-top-left)
          (event-loop nc (make-ibuff) (make-default-state))))))
